# Akelny WebSite

![Preview Img](screencapture.png)

Preview [Preview](https://akelny.netlify.app/)

## Quickstart

1. Install the [node.js](https://nodejs.org/en/)
2. Clone the project

    ```bash
    git clone https://Power50015@bitbucket.org/Power50015/akelny.git
    ```
3. Create firebase database (firestore & firebase Authentication)
4. Create firebase APP 
5. Go to project folder and add firebase app data to .env.example Template file and remove .example from the file name
3. Go run

    ```bash
    npm install
    ```

4. Start development mode

    ```bash
    npm run serve
    ```

5. In browser open page with address [http://localhost:8000/](http://localhost:8000/)

### Main tasks

- npm run serve -  launches watchers and server & compile project.
- npm run build - optimize & minify files for production version.

## INTRODUCTION & FEATURE

One of my first tries in vue 3, was a project for eduction purposes.
Creative template for Food Business Startups, web. All files and code have been well organized and nicely commented on for ease of customize.


## MAIN FEATURES :

- Valid HTML5, CSS3.
- Single Page APP.
- Vue 3 Composition.
- Fully Customizable.
- Clean Code.
- Fully Responsive.
- Firestore for database.


## FILES INCLUDED :

- Vue Files.
- JS Files.

## Credits

- Vue 3
- VueX 4
- Vue CLI
- Vue Router
- Bootstrab 5
- Google Fonts 
- Firestore

## Support:

- If you need any help using the file or need special customizing please contact me via my Bitbucket or my Website.
- If you like my html template, please follwo me , We’ll appreciate it very much Thank you.
